<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Торговая площадка Мой огород");
$APPLICATION->SetTitle("Элемент");?>
<div class="element-wrapper">
   <h1>Заголовок объявления</h1>

   <div class="info-about-item row">
      <p class="public-time col-lg-6 col-md-6 col-sm-12 col-xs-12">Размещено 3 сентября в 22:00</p>
      <p class="views-item col-lg-6 col-md-6 col-sm-12 col-xs-12"><span>Просмотров: </span>всего 1341/сегодня 23</p>
   </div>

   <div class="row">
      <div class="item-slider col-lg-5 col-md-5 col-sm-12 col-xs-12">
         <div class="slide-big-container">
            <div>
               <a class="fancybox-detail" rel="gallery" href="<?=SITE_TEMPLATE_PATH?>/images/demos/beach-cool-wallpaper-hd_1.jpg"><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/beach-cool-wallpaper-hd_1.jpg"></a>
            </div>
         </div>

         <div class="slide-controls-wrapper">
            <ul class="slide-controls">
               <li><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/beach-cool-wallpaper-hd_1.jpg"></li>
               <li><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/bicycle-1280x720.jpg"></li>
               <li><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/butterfly-wallpaper.jpeg"></li>
               <li><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/butterfly-wallpaper.jpeg"></li>
               <li><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/hd-wallpaper-1080p-22.jpg"></li>
               <li><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/HD-Wallpapers1.jpeg"></li>
               <li><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/i-should-buy-a-boat.jpg"></li>
               <li><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/ZhGEqAP.jpg"></li>
               <li><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/Amarok-logo-small.png"></li>
            </ul>
         </div>
      </div>
      <div class="element-properties col-lg-4 col-md-4 col-sm-12 col-xs-12">
         <div class="item-info-block">
            <div class="item-price">
               <p>1 000 руб</p>
            </div>
            <div class="item-vendor-name">
               <p>Продавец</p>
               <p>Армен брат Ахмеда</p>
            </div>
            <div class="item-vendor-phone">
               <p>Телефон</p>
               <p>+7 960 111-22-33</p>
            </div>
            <div class="item-vendor-city">
               <p>Город</p>
               <p>Москва</p>
            </div>
            <div class="item-vendor-address">
               <p>Адрес</p>
               <p>Черкизовский рынок</p>
            </div>
         </div>

         <table class="item-properties-list">
            <tbody>
               <tr>
                  <td>Свойство товара 1</td>
                  <td>Товар качественный</td>
               </tr>
               <tr>
                  <td>Свойство товара 2</td>
                  <td>Товар качественный</td>
               </tr>
               <tr>
                  <td>Свойство товара 3</td>
                  <td>Товар качественный</td>
               </tr>
               <tr>
                  <td>Свойство товара 4</td>
                  <td>А вот тут не факт</td>
               </tr>
            </tbody>
         </table>
      </div>
      <div class="right-sidebar-item col-lg-3 col-md-3 col-sm-12 col-xs-12">
         <div class="raiting-informer-wrapper col-lg-12 col-md-12 col-sm-6 col-xs-12">
            <ul class="raiting-informer">
            	<li class="informer-average">
            	<a class="rating-control" data-toggle="collapse" href="#collapseOne" aria-expanded="true"></a>
            		<div>
	            		<p class="raiting-item-name">Картофель</p>
	            		<p class="raiting-item-price">1.4 руб</p>
	            		<p class="arrow-indicator fa fa-caret-up"></p>
            		</div>
            		<ul id="collapseOne" class="panel-collapse collapse">
            			<li>
	            			<div>
			            		<p class="raiting-item-name">max</p>
			            		<p class="raiting-item-price">2 руб</p>
			            		<p class="arrow-indicator fa fa-caret-down"></p>
	            			</div>
            			</li>
            			<li>
	            			<div>
			            		<p class="raiting-item-name">min</p>
			            		<p class="raiting-item-price">1 руб</p>
			            		<p class="arrow-indicator fa fa-caret-up"></p>
	            			</div>
            			</li>
            		</ul>
            	</li>
            	<li class="informer-average">
            	<a class="rating-control" data-toggle="collapse" href="#collapseTwo" aria-expanded="true"></a>
            		<div>
	            		<p class="raiting-item-name">Морковь</p>
	            		<p class="raiting-item-price">1.4 руб</p>
	            		<p class="arrow-indicator fa fa-caret-up"></p>
	            		<ul id="collapseTwo" class="panel-collapse collapse">
	            			<li>
		            			<div>
				            		<p class="raiting-item-name">max</p>
				            		<p class="raiting-item-price">2 руб</p>
				            		<p class="arrow-indicator fa fa-caret-down"></p>
		            			</div>
	            			</li>
	            			<li>
		            			<div>
				            		<p class="raiting-item-name">min</p>
				            		<p class="raiting-item-price">1 руб</p>
				            		<p class="arrow-indicator fa fa-caret-down"></p>
		            			</div>
	            			</li>
            			</ul>
            		</div>
            	</li>
            	<li class="informer-average">
            	<a class="rating-control" data-toggle="collapse" href="#collapseThree" aria-expanded="true"></a>
            		<div>
	            		<p class="raiting-item-name">Капуста</p>
	            		<p class="raiting-item-price">1.5 руб</p>
	            		<p class="arrow-indicator fa fa-caret-up"></p>
	            		<ul id="collapseThree" class="panel-collapse collapse">
	            			<li>
		            			<div>
				            		<p class="raiting-item-name">max</p>
				            		<p class="raiting-item-price">2 руб</p>
				            		<p class="arrow-indicator fa fa-caret-down"></p>
		            			</div>
	            			</li>
	            			<li>
		            			<div>
				            		<p class="raiting-item-name">min</p>
				            		<p class="raiting-item-price">1 руб</p>
				            		<p class="arrow-indicator fa fa-caret-up"></p>
		            			</div>
	            			</li>
            			</ul>
            		</div>
            	</li>
            </ul>

         </div>
         <div class="banner-on-detail col-lg-12 col-md-12 col-sm-6 col-xs-12">
            <div class="vip-advert">
               <p class="vip-header-title">VIP-объявления</p>
               <ul>
                  <li>
                     <p class="vip-title">.NET developer</p>
                     <p class="vip-price">$2000</p>
                     <p class="vip-company">Huyak-Huyak and on production company</p>
                     <p class="vip-city">Москва</p>
                  </li>
                  <li>
                     <p class="vip-title">PHP developer</p>
                     <p class="vip-price">$2000</p>
                     <p class="vip-company">Huyak-Huyak and on production company</p>
                     <p class="vip-city">Москва</p>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="reviews-wrapper col-lg-9 col-md-9 col-sm-12 col-xs-12">
         <div class="review-item">
            <div class="review-left">
               <p class="reviewer-name">Александр</p>
               <p class="review-photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/Amarok-logo-small.png"></p>
               <p class="review-date">02.03.2016</p>
            </div>
            <div class="review-right">
               Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
               Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
               Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
               condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel,
            </div>
         </div>
         <div class="review-item">
            <div class="review-left">
               <p class="reviewer-name">Александр</p>
               <p class="review-photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/Amarok-logo-small.png"></p>
               <p class="review-date">02.03.2016</p>
            </div>
            <div class="review-right">
               Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
               Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
               Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
               condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel,
            </div>
         </div>
         <div class="review-item">
            <div class="review-left">
               <p class="reviewer-name">Александр</p>
               <p class="review-photo"><img src="<?=SITE_TEMPLATE_PATH?>/images/demos/Amarok-logo-small.png"></p>
               <p class="review-date">02.03.2016</p>
            </div>
            <div class="review-right">
               Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
               Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
               Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
               condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel,
            </div>
         </div>
      </div>
      <div class="banner-element col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
   </div>
</div>

<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>
