<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
checkAnonymRedirect();
$APPLICATION->SetTitle("Настройки");
?>

<?
$user = new CUser;

$currentUserId = $user->GetID();
$currentUser = CUser::GetByID($currentUserId)->Fetch();

$values = [];
if (!empty($_POST['submit'])) {
	if (!empty($_POST['user_name'])) {
		$values['NAME'] = $_POST['user_name'];
	}
	if (!empty($_POST['user_phone'])) {
		$values['PERSONAL_MOBILE'] = $_POST['user_phone'];
	}
  if (!empty($_POST['user_email'])) {
    $values['EMAIL'] = $_POST['user_email'];
  }
  if (!empty($_POST['company_name'])) {
    $values['UF_COMPANY_NAME'] = $_POST['company_name'];
  }
  if (!empty($_POST['uf_inn'])) {
    $values['UF_INN'] = $_POST['uf_inn'];
  }
  if (!empty($_POST['uf_ogrn'])) {
    $values['UF_OGRN'] = $_POST['uf_ogrn'];
  }
  if (!empty($_POST['uf_rch'])) {
    $values['UF_RCH'] = $_POST['uf_rch'];
  }
  if (!empty($_POST['uf_bank_name'])) {
    $values['UF_BANK_NAME'] = $_POST['uf_bank_name'];
  }
	if (!empty($_POST['uf_bic'])) {
    $values['UF_BIC'] = $_POST['uf_bic'];
  }
	if (!empty($_POST['uf_ks'])) {
    $values['UF_KS'] = $_POST['uf_ks'];
  }

	$user->Update($currentUserId, $values);	
}
$newUser = CUser::GetByID($currentUserId)->Fetch();
?>


<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/account_menu.php"
	)
);?>
<div class="accoun-settings-wrapper col-lg-6 col-md-6 col-sm-9 col-xs-12">
	<h1>Настройки</h1>
	<div class="settings-border-shadow">
		<div class="tabs-wrapper">
			<p class="col-xs-6 active"><a href="#common-setting" data-tab-id="common-setting">Общие</a></p>
			<p class="col-xs-6"><a href="#entity-setting" data-tab-id="entity-setting">Для юр. лиц</a></p>
		</div>
		<div class="clearfix"></div>
		<div class="account-settings-form visible" id="common-setting">
			<form method='post' action=''>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-user" aria-hidden="true"></i>
						<input type='text' name='user_name' value='<?=$newUser['NAME']?>' readonly='readonly' placeholder="Имя">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-mobile" aria-hidden="true"></i>
						<input type='text' name='user_phone' value='<?=$newUser['PERSONAL_MOBILE']?>' readonly='readonly' placeholder="Телефон">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<input type='text' name='user_email' value='<?=$newUser['EMAIL']?>' readonly='readonly' placeholder="E-mail">
					</span>
                    <span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="form-submit">
					<input type="submit" name="submit" value="Сохранить">
				</p>
			</form>
		</div>

		<div class="account-settings-form" id="entity-setting">
			<form method='post' action=''>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-user" aria-hidden="true"></i>
						<input type='text' name='user_name' value='<?=$newUser['NAME']?>' readonly='readonly' placeholder="Имя">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-mobile" aria-hidden="true"></i>
						<input type='text' name='user_phone' value='<?=$newUser['PERSONAL_MOBILE']?>' readonly='readonly' placeholder="Телефон">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<input type='text' name='user_email' value='<?=$newUser['EMAIL']?>' readonly='readonly' placeholder="E-mail">
					</span>
                    <span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-id-card" aria-hidden="true"></i>
						<input type='text' name='company_name' value='<?=$newUser['UF_COMPANY_NAME']?>' readonly='readonly' placeholder="Наименование организации">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-id-card" aria-hidden="true"></i>
						<input type='text' name='uf_inn' value='<?=$newUser['UF_INN']?>' readonly='readonly' placeholder="ИНН">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-id-card" aria-hidden="true"></i>
						<input type='text' name='uf_ogrn' value='<?=$newUser['UF_OGRN']?>' readonly='readonly' placeholder="ОГРН">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-id-card" aria-hidden="true"></i>
						<input type='text' name='uf_rch' value='<?=$newUser['UF_RCH']?>' readonly='readonly' placeholder="р/с">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-id-card" aria-hidden="true"></i>
						<input type='text' name='uf_bank_name' value='<?=$newUser['UF_BANK_NAME']?>' readonly='readonly' placeholder="в (наименование банка)">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-id-card" aria-hidden="true"></i>
						<input type='text' name='uf_bic' value='<?=$newUser['UF_BIC']?>' readonly='readonly' placeholder="БИК">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="account-settings-elem">
					<span class="form-input">
						<i class="fa fa-id-card" aria-hidden="true"></i>
						<input type='text' name='uf_ks' value='<?=$newUser['UF_KS']?>' readonly='readonly' placeholder="к/с">
					</span>
					<span class="form-buttons">
						<i class="fa fa-pencil pencil-icon" aria-hidden="true"></i>
					</span>
				</p>
				<p class="form-submit">
					<input type="submit" name="submit" value="Сохранить">
				</p>
			</form>
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/banner.php"
	)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
