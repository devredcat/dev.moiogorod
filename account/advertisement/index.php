<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
checkAnonymRedirect();
$APPLICATION->SetTitle("Мои объявления");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/account_menu.php"
	)
);?>

<?
if ($_GET['action']) {
  if (CModule::IncludeModule("iblock")) {
  	$element_id = $_GET['id'];
    $element = new CIBlockElement;

    $arSelect = array("PROPERTY_ITEM_USER", "ID");
	$arFilter = array("IBLOCK_ID" => 1, "ID" => $_GET['id'], "PROPERTY_ITEM_USER" => CUser::GetID());
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

    if (!empty($res->GetNextElement())) {	
		$element->SetPropertyValuesEx($element_id, 1, array('LAST_UPDATE' => ConvertTimeStamp(time(), 'FULL')));
	  }
  }
}
?>
<?$APPLICATION->IncludeComponent("bitrix:iblock.element.add.list", "add.list", Array(
	"ALLOW_DELETE" => "Y",	// Разрешать удаление
		"ALLOW_EDIT" => "Y",	// Разрешать редактирование
		"EDIT_URL" => "/account/add/",	// Страница редактирования элемента
		"ELEMENT_ASSOC" => "PROPERTY_ID",	// Привязка к пользователю
		"ELEMENT_ASSOC_PROPERTY" => "9",	// по свойству инфоблока -->
		"GROUPS" => "",	// Группы пользователей, имеющие право на добавление/редактирование
		"IBLOCK_ID" => "1",	// Инфоблок
		"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
		"MAX_USER_ENTRIES" => "100000",	// Ограничить кол-во элементов для одного пользователя
		"NAV_ON_PAGE" => "100",	// Количество элементов на странице
		"SEF_MODE" => "N",	// Включить поддержку ЧПУ
		"STATUS" => "ANY",	// Редактирование возможно
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/banner.php"
	)
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>