<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if ($_POST['senderid']) {
	$deleteTo = "UPDATE user_messages SET UF_IS_DELETED_TO=1 
			  WHERE UF_USER_ID_TO=" . CUser::GetID() . " AND UF_USER_ID_FROM=". $_POST['senderid'] . " AND UF_PRODUCT_ID=" . $_POST['itemid'];
	$DB->Query($deleteTo);

	$deleteFrom = "UPDATE user_messages SET UF_IS_DELETED_FROM=1  
			  WHERE UF_USER_ID_FROM=" . CUser::GetID() . " AND UF_USER_ID_TO=". $_POST['senderid'] . " AND UF_PRODUCT_ID=" . $_POST['itemid'];
	$DB->Query($deleteFrom);
}