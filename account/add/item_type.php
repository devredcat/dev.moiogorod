<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

const MY_HL_BLOCK_ID = 1;
//CModule::IncludeModule('highloadblock');
$entity_data_class = GetEntityDataClass(MY_HL_BLOCK_ID);
$rsData = $entity_data_class::getList(array(
    'select' => array('ID', 'UF_ITEM_TYPE'),
    'filter' => array('=UF_ACTIVE' => 1, 'UF_ITEM_TYPE' => '%'.$_GET['findStr'].'%')
));

$arItems = array();
while($el = $rsData->fetch()){
    $arItems[] = $el;
}
//echo "<pre>".print_r($arItems, true);
header('Content-Type: application/json');
echo json_encode($arItems);
