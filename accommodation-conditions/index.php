<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия размещения");
?>

<h1>Условия размещения объявлений на Moiogorod</h1>
<div class="accommodation-conditions">
	В настоящих Условиях размещения объявлений на Moiogorod («Условия») применяются термины, определенные <a href="/term-of-use">Пользовательским соглашением.</a><br>
	Настоящие Условия могут быть в любое время изменены Мой огород без уведомления Пользователя. Использование сервиса бесплатного размещения Объявлений после внесения соответствующих изменений означает безоговорочное согласие с ними Пользователя и устанавливает для него соответствующие обязательства и ответственность за соблюдение Условий.<br>

	<h3>1. Общие положения</h3>
	Мой огород предоставляет Пользователю возможность осуществлять размещение Объявлений о Товаре на Сайте Moiogorod.ru на условиях и с соблюдением требований, установленных настоящими Условиями, Пользовательским соглашением и иными правилами и документами, регламентирующими использование Moiogorod.<br>
	Размещение Объявлений осуществляется любым Пользователем, зарегистрированным в установленном Мой огород порядке на Сайте бесплатно, за исключением размещения Объявлений в отдельных категориях и регионах, размещаемых за плату в соответствии с Условиями размещения объявлений в отдельных категориях/регионах за плату.
	Все размещаемые Пользователем Объявления, возможность их редактирования, удаления, снятия с публикации, активации, совершение иных действий с Объявлением доступны Пользователю в Личном кабинете на Сайте.<br>
	Показ размещенных в соответствии с настоящими Условиями Объявлений осуществляется в результатах поиска в общем списке одновременно с Объявлениями иных Пользователей, схожих по содержанию, запросу или иным параметрам Объявления.<br>

	<h3>2. Условия размещения Объявлений и обязательства Пользователя</h3>
	Продавец обязуется размещать Объявления в соответствии с инструкциями на Moiogorod и предоставлять точную и полную информацию о Товаре и условиях его продажи (использования, приобретения и пр.). Размещая Объявление, Пользователь подтверждает, что он имеет право распоряжаться Товаром или осуществлять в отношении Товара иные действия, указанные в Объявлении.<br>
	В целях поддержания высокого качества Сервисов, Мой огород оставляет за собой право ограничить количество активных, т.е. доступных для просмотра третьими лицами Объявлений Пользователя на Moiogorod, а также ограничивать действия Пользователя на Moiogorod.<br>
	Пользователь имеет право реализовывать на Moiogorod принадлежащие ему Товары, при условии, что для этого не требуется специальных разрешений, а также при условии соблюдения действующего Пользовательского соглашения.<br>
	На Moiogorod запрещены Объявления о Товарах, продажа которых нарушает действующее законодательство Российской Федерации, противоречит общепринятым нормам морали, является оскорбительной или неуместной либо не соответствует политике Мой огород.<br>
	Пользователь обязан самостоятельно удостовериться, что продажа Товара не нарушает положений действующего законодательства и допускается Пользовательским соглашением, ознакомившись в том числе с содержанием перечня запрещенных товаров, основными причинами блокировки объявлений и учетных записей, требованиями к Объявлениям и другими документами, регламентирующими предоставление Сервисов на Сайте.<br>
	Продавец обязан тщательно проверять всю информацию о Товаре, указанном в Объявлении, и, в случае обнаружения неверной и/или неполной информации, добавлять необходимые сведения в описание и/или условия продажи Товара в Объявлении или исправлять неверную информацию, отредактировав Объявление.<br>
	Описание Товара, указанное Продавцом в Объявлении, его стоимость и предложение заключения сделки в отношении Товара составляют условия продажи этого Товара.
	В Объявлениях запрещено оставлять любые ссылки на страницы Интернет-сайтов, за исключением случаев, когда такие ссылки необходимы в целях выполнения требований действующего законодательства Российской Федерации.<br>
	Условия доставки должны включаться в условия продажи Товара, указанного в Объявлении. Условия продажи и описание Товара, указанные в Объявлении, не должны противоречить действующему законодательству и Пользовательскому соглашению, как на момент размещения Объявления, так и в дальнейшем, включая возможные изменения Объявления, изменение положений действующего законодательства Российской Федерации и иные обстоятельства.<br>
	Пользователь обязуется не включать в Объявления информацию об услугах, предоставляемых:<br>
	&nbsp •	Интернет-аукционами и/или сайтами, предлагающими товары и услуги, представленные на Moiogorod, за ту же либо меньшую плату;<br>
	&nbsp •	сайтами, предлагающими товары и услуги, запрещенные к продаже на Moiogorod.<br>
	Мой огород имеет право переместить, завершить или продлить срок демонстрации Объявления по техническим причинам, находящимся под контролем или вне контроля Мой огород.<br>
	Мой огород имеет право прекратить демонстрацию любого Объявления в любое время.<br>
	Объявления, размещенные Продавцом на Moiogorod, по единоличному решению Мой огород могут быть дополнительно опубликованы на Интернет-сайтах компаний — партнеров Мой огород. При размещении Объявлений Пользователей Мой огород вправе наносить на предоставленные Пользователем фотографии логотипы/водяные знаки Moiogorod или иных Интернет-сайтов компаний — партнеров Мой огород.<br>
	Пользователю запрещается размещать Объявления на Moiogorod, совершать или исполнять сделку с использованием Сервисов Moiogorod, которая может привести к нарушению Мой огород и/или Пользователем действующего законодательства Российской Федерации.<br>

	<h3>3. Предоставляемые Пользователем сведения при размещении Объявлений</h3>
	Сведения, предоставляемые Пользователем при размещении Объявлений должны быть полными, достоверными, соответствующими предлагаемому Товару и действительным намерениям Продавца в отношении такого Товара, не допускающими неоднозначного или двойного понимания.<br>
	<br>
	Ограничения.<br>
	Сведения, предоставленные Пользователем, в том числе при размещении Объявлений, и любые его действия на Moiogorod не должны:<br>
	&nbsp •	быть ложными, неточными или вводящими в заблуждение;<br>
	&nbsp •	способствовать мошенничеству, обману или злоупотреблению доверием;<br>
	&nbsp •	вести к совершению сделок с крадеными или поддельными предметами;<br>
	&nbsp •	нарушать или посягать на собственность третьего лица, его коммерческую тайну либо его право на неприкосновенность частной жизни;<br>
	&nbsp •	содержать сведения, оскорбляющие чью-либо честь, достоинство или деловую репутацию;<br>
	&nbsp •	содержать клевету или угрозы кому бы то ни было;<br>
	&nbsp •	призывать к совершению преступления, а также разжигать межнациональную рознь;<br>
	&nbsp •	способствовать, поддерживать или призывать к террористической и экстремистской деятельности;<br>
	&nbsp •	быть непристойными либо носить характер порнографии;<br>
	&nbsp •	содержать компьютерные вирусы, а также иные компьютерные программы, направленные, в частности, на нанесение вреда, неуполномоченное вторжение, тайный перехват либо присвоение данных любой системы, либо самой системы, либо ее части, либо личной информации или иных данных (включая данные Мой огород или иных Пользователей);<br>
	&nbsp •	причинять вред Мой огород, а также становиться причиной полной либо частичной потери Мой огород услуг провайдеров сети Интернет, либо услуг любых иных лиц;<br>
	&nbsp •	содержать материалы рекламного характера;<br>
	&nbsp •	нарушать интеллектуальные права третьих лиц, право на изображение гражданина и иные материальные и нематериальные права третьих лиц;<br>
	&nbsp •	иным образом нарушать действующее законодательство Российской Федерации.<br>

	<h3>4. Соответствие требованиям</h3>
	Мой огород может выборочно осуществлять проверку Объявлений на соответствие требованиям настоящих Условий и Пользовательского соглашения на любом этапе размещения Объявления, как в момент создания и отправки Объявления для публикации, так и в процессе его показа на Сайте.<br>
	При обнаружении в Объявлении признаков нарушения установленных Мой огород положений и требований действующего законодательства Российской Федерации Мой огород вправе отказать в размещении Объявления — прекратить показ Объявления, заблокировать его и предоставить Пользователю возможность устранить допущенные нарушения, заблокировать Объявления без возможности его редактирования, а также ограничить и/или заблокировать Пользователю доступ в его Личный кабинет.<br>

	<h3>5. Гарантии и ответственность</h3>
	Пользователь несет ответственность за соблюдение положений действующего законодательства Российской Федерации при использовании сервиса размещения Объявлений, предусмотренных настоящими Условиями, Пользовательским соглашением и иными положениями, установленными Мой огород и размещенными на Сайте.<br>
	Осуществляя размещение Объявления в соответствии с настоящими Условиями, Пользователь гарантирует, что обладает необходимыми правами на Товар, в отношении которого размещается предложение, на любые действия с таким Товаром, включая размещение соответствующей информации о Товаре на Сайте.<br>
	Пользователь единолично несет ответственность за реализацию Товаров, предложения о которых размещаются им на Moiogorod и любые ее последствия.
	Мой огород не является Продавцом и/или посредником сделок, совершаемых Пользователями исходя из информации, полученной на Moiogorod, в связи с чем не несет ответственности за любые сделки, заключенные между Пользователями при использовании Moiogorod, и их последствия.<br>
	Пользователь несет ответственность за соответствие Товара заявленным в Объявлении характеристикам, качеству, безопасности, а также за законность и возможность Продавца продать Товар.<br>
	Все претензии к Товару, к содержанию Объявления, информации Пользователя и любые иные требования к Продавцу в рамках заключаемых с таким Продавцом сделок, на основании информации, размещенной Продавцом на Сайте, разрешаются Продавцом своими силами и за свой счет, без участия Мой огород, поскольку Мой огород участником таких сделок не является.<br>
	В виду безвозмездности использования сервиса размещения Объявлений в соответствии с настоящими Условиями, положения законодательства о защите прав потребителей не применяются к отношениям между Мой огород и Пользователями, возникающим в связи с использованием Сайта и указанного в настоящих Условиях сервиса.<br>



</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>