<?php ini_set('max_execution_time', 3600);?>
<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog.php");?>

<?php
$json = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/parse/catalogparser/data/elementsData.json');
$json = json_decode($json, true);

foreach ($json as $value) {
	$element = new CIBlockElement;
	$price = $value['price'];
	if (strpos($price, '-')) {
		$position = strpos($price, '-');
		$price = substr($price, 0, $position);
	}
	$reg = '/[+0-9 \-()]+/';
	$phoneRaw = $value['contacts'];

	preg_match($reg, $phoneRaw, $phohes);

	$property = [
		'14' => $value['address'],
		'10' => $price,
		'2' => trim($phohes[0]),
		'6' => 2,
		'3' => $value['images']
	];

	$arLoadProductArray = Array(
	"ACTIVE_FROM" => date('d.m.Y H:i:s'), //17.09.2017 19:47:43
  "MODIFIED_BY"    => 1, // элемент изменен текущим пользователем
  "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
  "IBLOCK_ID"      => 1,
  "PROPERTY_VALUES"=> $property,
  "NAME"           => $value['title'],
  "ACTIVE"         => "Y",
  "PREVIEW_TEXT"   => $value['description'],
  "DETAIL_TEXT"    => "",
 );

	if($PRODUCT_ID = $element->Add($arLoadProductArray)) {
		echo "New ID: ".$PRODUCT_ID;
	}
	else {
	  echo "Error: ".$element->LAST_ERROR;
	}
}



?>
  