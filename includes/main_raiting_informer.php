<!--
Говядина , свинина , баранина, курица , молоко , яйца , огурец , помидор , хлеб, картофель , сыр , морковь , лук , чеснок
-->

<div class="main-informer-wrapper">
	<ul>
		<li>
			<p class="informer-tile-title">Говядина</p>
			<div class="rate-wrapper">
				<div class="rate-min">
					<p class="rate-title">мин</p>
					<p class="rate-value">35<span> руб</span></p>
				</div>
				<div class="rate-average">
					<p class="rate-title">сред</p>
					<p class="rate-value">40<span> руб</span></p>
				</div>
				<div class="rate-max">
					<p class="rate-title">макс</p>
					<p class="rate-value">45<span> руб</span></p>
				</div>
			</div>
		</li>
		<li>
			<p class="informer-tile-title">Свинина</p>
			<div class="rate-wrapper">
				<div class="rate-min">
					<p class="rate-title">мин</p>
					<p class="rate-value">35<span> руб</span></p>
				</div>
				<div class="rate-average">
					<p class="rate-title">сред</p>
					<p class="rate-value">40<span> руб</span></p>
				</div>
				<div class="rate-max">
					<p class="rate-title">макс</p>
					<p class="rate-value">45<span> руб</span></p>
				</div>
			</div>
		</li>
		<li>
			<p class="informer-tile-title">Курица</p>
			<div class="rate-wrapper">
				<div class="rate-min">
					<p class="rate-title">мин</p>
					<p class="rate-value">35<span> руб</span></p>
				</div>
				<div class="rate-average">
					<p class="rate-title">сред</p>
					<p class="rate-value">40<span> руб</span></p>
				</div>
				<div class="rate-max">
					<p class="rate-title">макс</p>
					<p class="rate-value">45<span> руб</span></p>
				</div>
			</div>
		</li>
		<li>
			<p class="informer-tile-title">Молоко</p>
			<div class="rate-wrapper">
				<div class="rate-min">
					<p class="rate-title">мин</p>
					<p class="rate-value">35<span> руб</span></p>
				</div>
				<div class="rate-average">
					<p class="rate-title">сред</p>
					<p class="rate-value">40<span> руб</span></p>
				</div>
				<div class="rate-max">
					<p class="rate-title">макс</p>
					<p class="rate-value">45<span> руб</span></p>
				</div>
			</div>
		</li>
		<li>
			<p class="informer-tile-title">Яйцо</p>
			<div class="rate-wrapper">
				<div class="rate-min">
					<p class="rate-title">мин</p>
					<p class="rate-value">35<span> руб</span></p>
				</div>
				<div class="rate-average">
					<p class="rate-title">сред</p>
					<p class="rate-value">40<span> руб</span></p>
				</div>
				<div class="rate-max">
					<p class="rate-title">макс</p>
					<p class="rate-value">45<span> руб</span></p>
				</div>
			</div>
		</li>
		<li>
			<p class="informer-tile-title">Огурец</p>
			<div class="rate-wrapper">
				<div class="rate-min">
					<p class="rate-title">мин</p>
					<p class="rate-value">35<span> руб</span></p>
				</div>
				<div class="rate-average">
					<p class="rate-title">сред</p>
					<p class="rate-value">40<span> руб</span></p>
				</div>
				<div class="rate-max">
					<p class="rate-title">макс</p>
					<p class="rate-value">45<span> руб</span></p>
				</div>
			</div>
		</li>
	</ul>
</div>