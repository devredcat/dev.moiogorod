<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"account.menu.mobile", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "account_menu",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "account_menu"
	),
	false
);?>