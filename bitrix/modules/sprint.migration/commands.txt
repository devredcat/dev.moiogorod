Все команды консоли:
  create                        Создать новую миграцию
    --prefix=<prefix>           Указать название
    --desc=<description>        Указать описанием

  status                        Показать суммарную информацию по миграциям
    --search=<text>             Отфильтровать по названию и описанию

  status <version>              Показать информацию по выбранной миграции

  list                          Показать список миграций
    --new                       Показать только новые
    --search=<text>             Отфильтровать по названию и описанию

  migrate                       Установить все миграции
    --up                        Установить все новые миграции (по умолчанию)
    --down                      Откатить все установленные миграции
    --search=<text>             Отфильтровать по названию и описанию

  up                            Установить новую миграцию (одну по умолчанию)
    --search=<text>             Отфильтровать по названию и описанию
    --all                       Установить все новые миграции

  up <version>                  Установить выбранную миграцию

  up <limit>                    Установить несколько новых миграций
    --search=<text>             Отфильтровать по названию и описанию

  down                          Откатить одну установленную миграцию (одну по умолчанию)
    --search=<text>             Отфильтровать по названию и описанию
    --all                       Откатить все установленные миграции

  down <version>                Откатить выбранную миграцию

  down <limit>                  Откатить несколько установленных миграций
    --search=<text>             Отфильтровать по названию и описанию

  execute <version>             Установить выбранную миграцию
    --up                        Установить выбранную миграцию
    --down                      Откатить выбранную миграцию

  redo <version>                Откатить и установить выбранную миграцию

  config                        Показать список конфигов
    --config=<name>             Переключить конфиг на выбранный

  help                          Показать справку