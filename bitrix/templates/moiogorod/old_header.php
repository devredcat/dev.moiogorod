<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE html>
<html>
	<head lang="ru">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?$APPLICATION->ShowTitle()?></title>
    
    <?$APPLICATION->ShowCSS();?>
    <!-- CSS  -->
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/bootstrap.css", true)?>
    <link href='http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css' rel='stylesheet' type='text/css'>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/reset.css", true)?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/main.css", true)?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/nik.css", true)?>
    <link href='/bitrix/templates/moiogorod/css/responsive.css' rel='stylesheet' type='text/css'>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/bootstrap-select.css", true)?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.bxslider.css", true)?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.fancybox.css", true)?>
    

    <!-- <link href="/bitrix/templates/moiogorod/css/bootstrap.css" rel="stylesheet" type='text/css'>
    <link href='http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css' rel='stylesheet' type='text/css'>
    <link href='/bitrix/templates/moiogorod/css/reset.css' rel='stylesheet' type='text/css'>
    <link href='/bitrix/templates/moiogorod/css/main.css' rel='stylesheet' type='text/css'>
    <link href='/bitrix/templates/moiogorod/css/responsive.css' rel='stylesheet' type='text/css'>
    <link href='/bitrix/templates/moiogorod/css/bootstrap-select.css' rel='stylesheet' type='text/css'>
    <link href='/bitrix/templates/moiogorod/css/jquery.bxslider.css' rel='stylesheet' type='text/css'>
    <link href='/bitrix/templates/moiogorod/css/jquery.fancybox.css' rel='stylesheet' type='text/css'>
    <link href='/bitrix/templates/moiogorod/css/nik.css' rel='stylesheet' type='text/css'> -->
    <!-- END CSS -->


    <?$APPLICATION->ShowHeadScripts();?>
    <!--JS -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="/bitrix/templates/moiogorod/js/bootstrap.js"></script>
    <script type="text/javascript" src="/bitrix/templates/moiogorod/js/bootstrap-select.min.js"></script>

    <script type="text/javascript" src="/bitrix/templates/moiogorod/js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="/bitrix/templates/moiogorod/js/jquery.fancybox.js"></script>
    <script type="text/javascript" src="/bitrix/templates/moiogorod/js/main.js"></script>
    <!-- END JS -->
    <?$APPLICATION->ShowHeadStrings()?>
    <?$APPLICATION->ShowHeadScripts();?>
    
		
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" /> 


	</head>
	<body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
		<div class="wrapper container"> <!-- container -->
   <header>
      <div class="row">
         <div class="logo col-lg-2 col-md-2 col-sm-3">
            <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo_icon.jpeg"></a>
         </div>
         <div class="intro-text-header col-lg-5 col-md-4">
            <span class="orange-text">Сайт фермерских объявлений №1 в России</span><br>
            На сайте over 9000 объявлений.
         </div>
         <div class="col-lg-3 col-md-3">
            <div class="user-acount">
               <i class="fa fa-user"></i>
               <a href="/account.php" class="to-profile">Личный кабинет</a>
               <i class="fa fa-angle-down" aria-hidden="true"></i>
               <div class="userinfo-hint">Вход и регистрация</div>
               <div class="useracount-popup">
                  <p class="popup-arrow"></p>
                  <a href="/" class="login-header btn btn-success">Войти</a>
                  <a href="/" class="singup-header btn btn-primary">Зарегистрироваться</a>
                  <ul class="userinfo-nav">
                     <li><a href="/account.php">Мои объявления</a></li>
                     <li><a href="/account.php">Кошелек</a></li>
                     <li><a href="/account.php">Сообщения</a></li>
                     <li><a href="/account.php">Настройки</a></li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="add-to-advert col-lg-2 col-md-3 col-sm-5 col-xs-8">
            <a href="/addform.php" class="btn btn-success">Подать объявление</a>
         </div>
         <div class="search-button-header col-sm-2 col-xs-2">
            <a href="#" data-toggle="collapse" data-target="#search-form-global">
               <i class="fa fa-search" aria-hidden="true"></i>
            </a>
         </div>
         <div class="account-button-header col-sm-2 col-xs-2">
            <a href="#">
               <i class="fa fa-user" aria-hidden="true"></i>
            </a>
         </div>
      </div>
      <!--<div class="row">
         <div id="search-input" class="search-form-header collapse col-sm-12 col-xs-12">
            <input type="text" value="" placeholder="Что ищем?">
            <i class="fa fa-search" aria-hidden="true"></i>
         </div>
      </div>-->
      <?php if($_SERVER['REQUEST_URI'] == '/') :?>
         <div class="category-header">
            <div class="row">
               <div class="category-menu-control" data-toggle="collapse" data-target="#category-header-list">
                  <div class="category-title col-sm-12 col-xs-12">
                    <p>Категории</p>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                  </div>
                  
               </div>
            </div>
            <div id="category-header-list" class="category-list collapse">
               <div class="row">
                  <div class="item col-lg-3 col-md-3 col-sm-12 col-xs-12">
                     <a href="/sections.php">
                        <i class="category-icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_icons/menu_point_1.jpg" alt=""/></i>
                        <span class="category-header-title">Животноводство</span>
                     </a>
                  </div>
                  <div class="item col-lg-3 col-md-3 col-sm-12 col-xs-12">
                     <a href="/sections.php">
                        <i class="category-icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_icons/menu_point_2.jpg" alt=""/></i>
                        <span class="category-header-title">Растениеводство</span>
                     </a>
                  </div>
                  <div class="item col-lg-3 col-md-3 col-sm-12 col-xs-12">
                     <a href="/sections.php">
                        <i class="category-icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_icons/menu_point_3.jpg" alt=""/></i>
                        <span class="category-header-title">Сельхозтехника</span>
                     </a>
                  </div>
                  <div class="item col-lg-3 col-md-3 col-sm-12 col-xs-12">
                     <a href="/sections.php">
                        <i class="category-icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_icons/menu_point_4.jpg" alt=""/></i>
                        <span class="category-header-title">Хозяйство</span>
                     </a>
                  </div>
               </div>
               <div class="row">
                   <div class="item col-lg-3 col-md-3 col-sm-12 col-xs-12">
                       <a href="/sections.php">
                           <i class="category-icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_icons/menu_point_4.jpg" alt=""/></i>
                           <span class="category-header-title">Хозяйство</span>
                       </a>
                   </div>
                   <div class="item col-lg-3 col-md-3 col-sm-12 col-xs-12">
                       <a href="/sections.php">
                           <i class="category-icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_icons/menu_point_3.jpg" alt=""/></i>
                           <span class="category-header-title">Сельхозтехника</span>
                       </a>
                   </div>
                   <div class="item col-lg-3 col-md-3 col-sm-12 col-xs-12">
                       <a href="/sections.php">
                           <i class="category-icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_icons/menu_point_2.jpg" alt=""/></i>
                           <span class="category-header-title">Растениеводство</span>
                       </a>
                   </div>
                   <div class="item col-lg-3 col-md-3 col-sm-12 col-xs-12">
                       <a href="/sections.php">
                           <i class="category-icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_icons/menu_point_1.jpg" alt=""/></i>
                           <span class="category-header-title">Животноводство</span>
                       </a>
                   </div>
               </div>
            </div>
         </div>
      <?php endif; ?>

      <div id="search-form-global" class="global-search collapse">
         <div class="row">
            <form method="post">
               <div class="category-search col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <select class="selectpicker">
                     <option>Животноводство</option>
                     <option>Растениеводство</option>
                     <option>Готовая продукция</option>
                     <option>Сельхозтехника</option>
                     <option>Недвижимость</option>
                     <option>Хозяйство</option>
                  </select>
               </div>

               <div class="items-input-search col-lg-6 col-md-5 col-sm-12 col-xs-12">
                  <input type="text" placeholder="Поиск по объявлениям">
               </div>

               <div class="clearfix-sm-xs"></div>

               <div class="city-search col-lg-3 col-md-3 col-sm-12 col-xs-12" >
                  <select class="selectpicker" data-live-search="true">
                     <option>По всей России</option>
                     <option>Масквэ</option>
                     <option>Краснодар</option>
                     <option>Уфа</option>
                     <option>Великий Новгород</option>
                     <option>Магадан</option>
                     <option>Масквэ</option>
                     <option>Краснодар</option>
                     <option>Уфа</option>
                     <option>Великий Новгород</option>
                     <option>Магадан</option>
                     <option>Масквэ</option>
                     <option>Краснодар</option>
                     <option>Уфа</option>
                     <option>Великий Новгород</option>
                     <option>Магадан</option>
                     <option>Масквэ</option>
                     <option>Краснодар</option>
                     <option>Уфа</option>
                     <option>Великий Новгород</option>
                     <option>Магадан</option>
                  </select>
               </div>

               <div class="search-submit col-lg-1 col-md-2 col-sm-12 col-xs-12">
                  <button type="button">Найти</button>
               </div>
            </form>
         </div>
      <div>
   </header>
   <div class="content">
	
						