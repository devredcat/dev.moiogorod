<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
  die();
?>
</div> <!-- content end -->
</div><!-- container end-->
</div> <!-- wrapper end -->

<div class="clearfix"></div>

<!-- informer on main page -->
<?/* $APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "inc",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/includes/main_raiting_informer.php"
  )
); */?>
<!-- informer on main page end -->

<footer>
  <div class="container">
    <?$APPLICATION->IncludeComponent(
      "bitrix:menu",
      "footer.menu",
      Array(
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "left",
        "DELAY" => "N",
        "MAX_LEVEL" => "1",
        "MENU_CACHE_GET_VARS" => array(0=>"",),
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "ROOT_MENU_TYPE" => "footer",
        "USE_EXT" => "N"
      )
    );?>
  </div>
</footer>
<div class="copyright">
    <p class="copyright-text">&copy; 2016 Мой огород</p>
</div>
<div class="overlay"></div>
<? if (!empty($debug)) : ?>
  <? foreach ($debug as $arrDebug): ?>
    <pre style="font-size: 12px">
         <? print_r($arrDebug); ?>
      </pre>
    <hr>
  <? endforeach; ?>
<? endif ?>

</body>
</html>