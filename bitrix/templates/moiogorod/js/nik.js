/**
 * Created by celldweller on 11-Feb-17.
 */

$(document).ready(function () {

  // Get user geoposition
  $('.geoposition-button').on('click', function () {
    navigator.geolocation.getCurrentPosition(function (position) {
      var userCoord = position.coords.latitude + '&' + position.coords.longitude;
      //var userCoord = 53.861167599999995 + '&' + 27.4763641;
      var date = new Date(new Date().getTime() + 3600 * 1000 * 24 * 30);
      document.cookie = "userGeoposition=" + userCoord + "; path=/; expires=" + date.toUTCString();
      window.location.replace($('.geoposition-button').data('url'));
    });
  });

  // Control items for setting fields.
  $('.account-settings-form .pencil-icon').on('click', function () {
    $(this).parents('p').find('input[type="text"]').removeAttr('readonly');
    $(this).parents('p').find('input[type="text"]').removeAttr('placeholder');
    $(this).hide();
  });

  // tabs control on account page.
  $('.tabs-wrapper p').on('click', function (e) {
    e.preventDefault();
    $('.tabs-wrapper p').removeClass('active');
    $(this).addClass('active');
    var tabId = $(this).find('a').data('tab-id');
    $('.accoun-settings-wrapper').find('.account-settings-form').removeClass('visible');
    $('.accoun-settings-wrapper').find('#' + tabId).addClass('visible');
  });

  // Search input on mobile screen
  $('.search-icon-mobile').on('click', function () {
    $('.search-global-mobile').toggle();
    $(this).toggleClass('search-open');
  });

  // Filling E-mail field by admin field in registration form.
  /*$('.bx-auth-reg input[name="REGISTER[LOGIN]"]').blur(function () {
   var emailValue = $(this).val();
   $('.bx-auth-reg input[name="REGISTER[EMAIL]"]').attr('value', emailValue);
   });*/

  /* Main menu left sidebar */
  // Expand current menu items
  $('.main-left-item.current').find('.main-left-nested-wrapper').removeClass('menu-close');
  $('.main-left-item.current').find('.menu-item-arrow').addClass('rotate-180');

  $('.first-level-link').on('click', function () {
    var dataTarget = $(this).find('.main-left-section-link').attr('data-target');
    $('.main-left-wrapper').find('.menu-item-arrow').removeClass('rotate-180');
    $('.main-left-wrapper').find('div.main-left-nested-wrapper:not(' + dataTarget + ')').addClass('menu-close');
    if ($(this).siblings('div' + dataTarget).hasClass('menu-close')) {
      $(this).siblings('div' + dataTarget).removeClass('menu-close');
      $(this).find('.menu-item-arrow').addClass('rotate-180');
    } else {
      $(this).siblings('div' + dataTarget).addClass('menu-close');
      $('.main-left-item').find('.menu-item-arrow').removeClass('rotate-180');
    }
  })

  // Popup for messages
  $('.messages-list li div').on('click', function () {
    var messageId = $(this).parent('li').data('messid');
    var senderId = $(this).parent('li').data('senderid');
    var isRead = $(this).parent('li').data('isread');
    $(this).parent('li').attr("data-isread", 1);
    $.fancybox.showLoading();
    $.ajax({
      url: '/account/messages/getAllMessages.php',
      data: {messageId: messageId, senderId: senderId, isRead: isRead},
      method: 'POST',
      success: function (response) {
        $.fancybox.open({
          content: response,
        });
      }
    });
  });

  // Delete particular message thread
  $('.message-remove').on('click', function () {
    var senderId = $(this).parent('li').data('senderid');
    var itemId = $(this).parent('li').data('itemid');
    $(this).parent('li').remove();
    if ($('.messages-list li').length == 0) {
      $('.messages-list').append('<p class="no-messages">У вас нет сообщений</p>');
    }
    $.ajax({
      url: '/account/messages/deleteMessage.php',
      data: {senderid: senderId, itemid: itemId},
      method: 'POST',
      success: function (response) {
      }
    });
  });

  // Send confirmation code
  $('.bx-auth-reg input[name="register_submit_button"]').on('click', function () {
    var userPhone = "<?=$curentUser?>";
    $(this).parent('li').remove();
    $.ajax({
      url: '/account/messages/deleteMessage.php',
      data: {senderid: senderId, itemid: itemId},
      method: 'POST',
      success: function (response) {
      }
    });
  });
});