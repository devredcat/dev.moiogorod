<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="account-menu col-lg-3 col-md-3 hidden-sm hidden-xs">
  <?if (!empty($arResult)):?>
    <ul class="left-menu">
      <?
      $i = 1;
      $lastElement = count($arResult);
      foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
          continue;
      ?>
        <?if($arItem["SELECTED"]):?>
          <li>
            <p class="account-menu-image">
              <?switch ($i) {
                case 1 :
                  echo '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                  break;
                case 2 :
                  echo '<i class="fa fa-credit-card-alt" aria-hidden="true"></i>';
                  break;
                case 3 :
                  echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
                  break;
                case 4 :
                  echo '<i class="fa fa-cog" aria-hidden="true"></i>';
                  break;
              }?>
              <!--<img src="<?/*= SITE_TEMPLATE_PATH */?>/images/account_icon/account_image_<?/*=$i*/?>.png">-->
            </p>
            <p class="account-menu-title">
              <a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a>
              <span class="accoun-menu-arrow">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
              </span>
            </p>
          </li>
        <?elseif ($lastElement == $i) :?>
          <li>
            <p class="account-menu-image">
              <i class="fa fa-sign-out" aria-hidden="true"></i>
            </p>
            <p class="account-menu-title">
              <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
            </p>
          </li>
        <?else:?>
          <li>
            <p class="account-menu-image">
              <?switch ($i) {
                case 1 :
                  echo '<i class="fa fa-list-alt" aria-hidden="true"></i>';
                  break;
                case 2 :
                  echo '<i class="fa fa-credit-card-alt" aria-hidden="true"></i>';
                  break;
                case 3 :
                  echo '<i class="fa fa-envelope" aria-hidden="true"></i>';
                  break;
                case 4 :
                  echo '<i class="fa fa-cog" aria-hidden="true"></i>';
                  break;
              }?>
            </p>
            <p class="account-menu-title">
              <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
            </p>
          </li>
        <?endif?>
        <?$i++;?>
      <?endforeach?>
    </ul>
  <?endif?>
</div>
