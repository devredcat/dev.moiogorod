<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init();
?>

<div class="bx-system-auth-form<?=($arResult["FORM_TYPE"] == "logout") ? ' logged': ''?>">
  <? if ($arResult["FORM_TYPE"] == "login" || strpos($APPLICATION->GetCurPage(), 'confirm')): ?>
    <div class="account-control-button">
      <p class="account-login">
        <a href="/account/login">Вход</a>
      </p>
      <p class="account-register">
        <a href="/account/registration">Регистрация</a>
      </p>
    </div>
  <? else: ?>
    <form action="<?= $arResult["AUTH_URL"] ?>">
      <div class="account-info-header-wrapper">
        <div class="user-info-header">
          <p class="user-name-header">
            <a href="<?= $arResult["PROFILE_URL"] ?>" title="<?= GetMessage("AUTH_PROFILE") ?>">
              <?= $arResult["USER_NAME"] ?><br/>
            </a>
          </p>
          <div class="user-balance-header">
            <p class="balance-title">Баланс: </p>
            <p class="balance-amount">0 <span>&#8381;</span></p>
          </div>
          <!--<p class="logout-button">
          <? /* foreach ($arResult["GET"] as $key => $value): */ ?>
            <input type="hidden" name="<? /*= $key */ ?>" value="<? /*= $value */ ?>"/>
          <? /* endforeach */ ?>
          <input type="hidden" name="logout" value="yes"/>
          <input type="submit" name="logout_butt" value="<? /*= GetMessage("AUTH_LOGOUT_BUTTON") */ ?>"/>
        </p>-->
        </div>
      </div>
      <div class="account-menu-header-control">
        <p class="account-arrow">
          <i class="fa fa-chevron-down" aria-hidden="true"></i>
        </p>
      </div>
    </form>
  <? endif ?>
</div>
