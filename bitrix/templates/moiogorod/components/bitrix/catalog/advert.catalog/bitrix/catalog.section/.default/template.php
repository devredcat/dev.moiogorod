<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
  die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
  <div class="page-title-filter">
    <h1><?= $arResult['NAME'] ?></h1>
    <div class="filter-button">
      <img src="<?= SITE_TEMPLATE_PATH ?>/images/catalog_icons/section_list.jpg"/>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="item-list">
    <? if (!empty($arResult['ITEMS'])) : ?>
      <? foreach ($arResult['ITEMS'] as $key => $arItem) : ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strSectionEdit);
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams); ?>
        <? $picture = CFile::ResizeImageGet($arItem['PROPERTIES']['ADDITIONAL_PHOTO']['VALUE'][0], ['width' => '165', 'height' => '180'], BX_RESIZE_IMAGE_EXACT, true, [], false, "80"); ?>
        <div id="<? echo $this->GetEditAreaId($arItem['ID']); ?>" class="element-item">
          <div class="item-image col-xs-4">
            <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
              <img
                src="<?= (!empty($picture)) ? $picture['src'] : SITE_TEMPLATE_PATH . '/images/no_image_photo.png' ?>">
            </a>
          </div>
          <div class="item-addinfo col-xs-8">
            <p class="item-title">
              <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                <?= TruncateText(strip_tags($arItem['NAME']), 90) ?>
              </a>
            </p>
            <!--<p class="element-price"><? /*=$arItem['PROPERTIES']['ITEM_COST']['VALUE'] . ' руб.'*/ ?></p>-->
            <?if (!empty($arItem['PROPERTIES']['ITEM_COST']['VALUE'])) :?>
              <p class="element-price"><?= CurrencyFormat($arItem['PROPERTIES']['ITEM_COST']['VALUE'], "RUB"); ?></p>
            <?endif?>
            <p class="item-description">
              <?if (!empty($arItem['DETAIL_TEXT'])) :?>
                <?= TruncateText(strip_tags($arItem['DETAIL_TEXT']), 140) ?>
              <?else :?>
                <?= TruncateText(strip_tags($arItem['PREVIEW_TEXT']), 140) ?>
              <?endif?>
            </p>

            <div class="item-distance-date">
              <? if ($_COOKIE['userGeoposition']) : ?>
                <p class="item-distance col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <? if ($arItem['ITEM_DISTANCE'] > 1000) {
                    if ($arItem['ITEM_DISTANCE'] % 1000 == 0) {
                      echo $arItem['ITEM_DISTANCE'] / 1000 . ' км';
                    } else {
                      $distance_metre = ($arItem['ITEM_DISTANCE'] % 1000) / 100;
                      echo floor($arItem['ITEM_DISTANCE'] / 1000) . ' км ' . round($distance_metre) * 100 . ' м';
                    }
                  } else {
                    $distance_metre = $arItem['ITEM_DISTANCE'] / 100;
                    echo round($distance_metre) * 100 . ' м';
                  } ?>
                </p>
              <? endif; ?>
              <p class="publication-date <?=($_COOKIE['userGeoposition']) ? "col-lg-6 col-md-6 col-sm-6 col-xs-12" : "col-xs-12"?>"><?= getPublicationDate($arItem['PROPERTIES']['LAST_UPDATE']['VALUE']) ?></p>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      <? endforeach; ?>
    <? else : ?>
      <p>Товаров не найдено</p>
    <? endif; ?>
  </div>

<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
  <div class="catalog-list-pager col-xs-12"><?= $arResult["NAV_STRING"] ?></div>
<? endif ?>