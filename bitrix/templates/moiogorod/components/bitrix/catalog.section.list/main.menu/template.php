<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>
<div class="mainmenu-mainpage row">
  <?foreach ($arResult['SECTIONS'] as $arSection) :?>
    <div class="mainmenu-element-wrapper col-lg-3 col-md-3 col-sm-4 col-xs-12">
      <div class="mainmenu-element">
        <a class="mainmenu-link" href="<?=$arSection['SECTION_PAGE_URL']?>">
          <p class="mainmenu-img">
            <img src="<?=$arSection['PICTURE']['SRC']?>">
          </p>
          <p class="mainmenu-title"><?=$arSection['NAME']?></p>
        </a>
      </div>
    </div>
  <?endforeach;?>
</div>
<div class="clearfix"></div>
