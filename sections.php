<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Торговая площадка Мой огород");
$APPLICATION->SetTitle("Раздел");?>

<div class="list-control">
   <div class="btn-group grid-checkbox" data-toggle="buttons">
      <label class="btn btn-primary active">
         <input type="radio" name="options" id="option1" autocomplete="off" checked>
         <i class="fa fa-th" aria-hidden="true" data-view="tile-view"></i>
      </label>
      <label class="btn btn-primary">
         <input type="radio" name="options" id="option2" autocomplete="off">
         <i class="fa fa-th-list" aria-hidden="true" data-view="list-view"></i>
      </label>
   </div>

   <div class="section-items-filter">
      <select class="selectpicker">
         <option>По дате</option>
         <option>Дешевле</option>
         <option>Дороже</option>
      </select>
   </div>

</div>
<div class="catalog-list-wrapper">
   <div class="row">
      <div class="catalog-list tile-view col-lg-8 col-md-8 col-sm-12 col-xs-12">
         <div class="element-item-wrapper row">
            <div class="element-item col-lg-4 col-md-4 col-sm-12 col-xs-12">
               <p class="advertisment-image">
                  <a href="/element.php">
                     <img src="<?=SITE_TEMPLATE_PATH?>/images/sobaken.jpg">
                  </a>
               </p>
               <div class="item-info-wrapper">
                  <p class="advertisment-title selected-ads">
                     <a href="/element.php">Свинятина</a>
                  </p>
                  <p class="advert-price">1500 000<span> руб.</span></p>
                  <p class="advert-company">OOO Хрю</p>
                  <p class="advert-city">Москва</p>
                  <p class="advert-time">Сегодня 11.30</p>
               </div>
            </div>
            <div class="element-item col-lg-4 col-md-4 col-sm-12 col-xs-12">
               <p class="advertisment-image">
                  <a href="/element.php">
                     <img src="<?=SITE_TEMPLATE_PATH?>/images/sobaken.jpg">
                  </a>
               </p>
               <div class="item-info-wrapper">
                  <p class="advertisment-title">
                     <a href="/element.php">Свинятина</a>
                  </p>
                  <p class="advert-price">1500 000<span> руб.</span></p>
                  <p class="advert-additional-text">
                     Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem
                  </p>
                  <p class="advert-company">OOO Хрю</p>
                  <p class="advert-city">Москва</p>
                  <p class="advert-time">Сегодня 11.30</p>
               </div>
            </div>
            <div class="element-item col-lg-4 col-md-4 col-sm-12 col-xs-12">
               <p class="advertisment-image">
                  <a href="/element.php">
                     <img src="<?=SITE_TEMPLATE_PATH?>/images/sobaken.jpg">
                  </a>
               </p>
               <div class="item-info-wrapper">
                  <p class="advertisment-title">
                     <a href="/element.php">Свинятина</a>
                  </p>
                  <p class="advert-price">1500 000<span> руб.</span></p>
                  <p class="advert-company">OOO Хрю</p>
                  <p class="advert-city">Москва</p>
                  <p class="advert-time">Сегодня 11.30</p>
               </div>
            </div>
            <div class="element-item col-lg-4 col-md-4 col-sm-12 col-xs-12">
               <p class="advertisment-image">
                  <a href="/element.php">
                     <img src="<?=SITE_TEMPLATE_PATH?>/images/sobaken.jpg">
                  </a>
               </p>
               <div class="item-info-wrapper">
                  <p class="advertisment-title">
                     <a href="/element.php">Свинятина</a>
                  </p>
                  <p class="advert-price">1500 000<span> руб.</span></p>
                  <p class="advert-company">OOO Хрю</p>
                  <p class="advert-city">Москва</p>
                  <p class="advert-time">Сегодня 11.30</p>
               </div>
            </div>
         </div>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
         <div class="vip-advert">
            <p class="vip-header-title">VIP-объявления</p>
            <ul>
               <li>
                  <p class="vip-title">.NET developer</p>
                  <p class="vip-price">$2000</p>
                  <p class="vip-company">Huyak-Huyak and on production company</p>
                  <p class="vip-city">Москва</p>
               </li>
               <li>
                  <p class="vip-title">PHP developer</p>
                  <p class="vip-price">$2000</p>
                  <p class="vip-company">Huyak-Huyak and on production company</p>
                  <p class="vip-city">Москва</p>
               </li>
            </ul>
         </div>
      </div>
   </div>
</div>

<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>